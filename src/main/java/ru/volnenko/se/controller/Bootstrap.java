package ru.volnenko.se.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.error.CommandCorruptException;
import ru.volnenko.se.event.CommandEvent;

import java.util.*;

/**
 * @author Denis Volnenko
 */
@Component("bootstrap")
public final class Bootstrap {

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Scanner scanner = new Scanner(System.in);

    private List<AbstractCommand> commandList;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Autowired
    public void setCommandList(List<AbstractCommand> commandList) {
        this.commandList = commandList;
    }

    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty()) throw new CommandCorruptException();
        commands.put(cliCommand, command);
    }

    public void registry() {
        commandList.forEach(this::registry);
    }

    public void init() throws Exception {
        registry();
        start();
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = scanner.nextLine();
            publish(command);
        }
    }

    public void publish(String command) {
        publisher.publishEvent(new CommandEvent(this, command));
    }

    public List<AbstractCommand> getListCommand() {
        return new ArrayList<>(commands.values());
    }

    public String nextLine() {
        return scanner.nextLine();
    }

    public Integer nextInteger() {
        final String value = nextLine();
        if (value == null || value.isEmpty()) return null;
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            return null;
        }
    }
}
