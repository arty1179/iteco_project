package ru.volnenko.se;

import org.springframework.context.support.GenericXmlApplicationContext;
import ru.volnenko.se.command.system.HelpCommand;
import ru.volnenko.se.controller.Bootstrap;

public class App {
    public static void main(String[] args) throws Exception {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.load("classpath:META-INF/spring/app-context-xml.xml");
        context.refresh();
        final Bootstrap bootstrap = (Bootstrap) context.getBean("bootstrap");
        System.out.println(context.getBean(HelpCommand.class).hashCode());
        bootstrap.init();
    }

}
