package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.controller.Bootstrap;
import ru.volnenko.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component("project-create")
public final class ProjectCreateCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @Autowired
    private IProjectService projectService;

    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = bootstrap.nextLine();
        projectService.createProject(name);
        System.out.println("[OK]");
        System.out.println();
    }

    @EventListener(condition = "#commandEvent.command.toLowerCase().contains('project-create')")
    public void onApplicationEvent(CommandEvent commandEvent) {
        execute();
    }
}
