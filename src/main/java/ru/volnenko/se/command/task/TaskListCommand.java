package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component("task-list")
public final class TaskListCommand extends AbstractCommand {

    @Autowired
    private ITaskService taskService;

    @Override
    public String command() {
        return "task-list";
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        int index = 1;
        for (Task task : taskService.getListTask()) {
            System.out.println(index + ". " + task.getName());
            index++;
        }
        System.out.println();
    }

    @EventListener(condition = "#commandEvent.command.toLowerCase().contains('task-list')")
    public void onApplicationEvent(CommandEvent commandEvent) {
        execute();
    }
}
