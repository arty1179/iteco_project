package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.controller.Bootstrap;
import ru.volnenko.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component("task-remove")
public final class TaskRemoveCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @Override
    public String command() {
        return "task-remove";
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVING TASK]");
        System.out.println("Enter task order index:");
        final Integer orderIndex = bootstrap.nextInteger();
        if (orderIndex == null) {
            System.out.println("Error! Incorrect order index...");
            System.out.println();
            return;
        }
        System.out.println("[OK]");
    }

    @EventListener(condition = "#commandEvent.command.toLowerCase().contains('task-remove')")
    public void onApplicationEvent(CommandEvent commandEvent) {
        execute();
    }

}
