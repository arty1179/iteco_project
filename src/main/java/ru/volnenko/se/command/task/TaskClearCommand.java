package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.event.CommandEvent;

/**
 * @author Denis Volnenko
 */
@Component("task-clear")
public final class TaskClearCommand extends AbstractCommand {

    @Autowired
    private ITaskService taskService;
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public String command() {
        return "task-clear";
    }

    @Override
    public void execute() {
        taskService.clear();
        System.out.println("[ALL TASK REMOVED]");
    }

    @EventListener(condition = "#commandEvent.command.toLowerCase().contains('task-clear')")
    public void onApplicationEvent(CommandEvent commandEvent) {
        execute();
    }
}
