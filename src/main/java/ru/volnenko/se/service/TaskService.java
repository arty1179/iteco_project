package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public final class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository iTaskRepository;

    @Autowired
    private IProjectRepository iProjectRepository;

    @Override
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        return iTaskRepository.createTask(name);
    }

    @Override
    public Task getTaskById(final String id) {
        return iTaskRepository.getTaskById(id);
    }

    @Override
    public Task merge(final Task task) {
        return iTaskRepository.merge(task);
    }

    @Override
    public void removeTaskById(final String id) {
        iTaskRepository.removeTaskById(id);
    }

    @Override
    public List<Task> getListTask() {
        return iTaskRepository.getListTask();
    }

    @Override
    public void clear() {
        iTaskRepository.clear();
    }

    @Override
    public Task createTaskByProject(final String projectId, final String taskName) {
        final Project project = iProjectRepository.getProjectById(projectId);
        if (project == null) return null;
        final Task task = iTaskRepository.createTask(taskName);
        task.setProjectId(project.getId());
        return task;
    }

    @Override
    public Task getByOrderIndex(Integer orderIndex) {
        return iTaskRepository.getByOrderIndex(orderIndex);
    }

    @Override
    public void merge(Task... tasks) {
        iTaskRepository.merge(tasks);
    }

    @Override
    public void load(Task... tasks) {
        iTaskRepository.load(tasks);
    }

    @Override
    public void load(Collection<Task> tasks) {
        iTaskRepository.load(tasks);
    }

    @Override
    public void removeTaskByOrderIndex(Integer orderIndex) {
        iTaskRepository.removeTaskByOrderIndex(orderIndex);
    }

}
