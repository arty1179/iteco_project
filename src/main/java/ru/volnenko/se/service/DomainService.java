package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.service.IDomainService;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Domain;

/**
 * @author Denis Volnenko
 */
@Service
public final class DomainService implements IDomainService {

    @Autowired
    private IProjectService iProjectService;

    @Autowired
    private ITaskService iTaskService;

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        iProjectService.load(domain.getProjects());
        iTaskService.load(domain.getTasks());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(iProjectService.getListProject());
        domain.setTasks(iTaskService.getListTask());
    }

}
