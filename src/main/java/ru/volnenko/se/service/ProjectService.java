package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.entity.Project;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public final class ProjectService implements ru.volnenko.se.api.service.IProjectService {

    @Autowired
    private IProjectRepository iProjectRepository;

    @Override
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        return iProjectRepository.createProject(name);
    }

    @Override
    public Project merge(final Project project) {
        return iProjectRepository.merge(project);
    }

    @Override
    public Project getProjectById(final String id) {
        return iProjectRepository.getProjectById(id);
    }

    @Override
    public void removeProjectById(final String id) {
        iProjectRepository.removeProjectById(id);
    }

    @Override
    public List<Project> getListProject() {
        return iProjectRepository.getListProject();
    }

    @Override
    public void clear() {
        iProjectRepository.clear();
    }

    @Override
    public void merge(Project... projects) {
        iProjectRepository.merge(projects);
    }

    @Override
    public void load(Collection<Project> projects) {
        iProjectRepository.load(projects);
    }

    @Override
    public void load(Project... projects) {
        iProjectRepository.load(projects);
    }

    @Override
    public Project removeByOrderIndex(Integer orderIndex) {
        if (orderIndex == null) return null;
        return iProjectRepository.removeByOrderIndex(orderIndex);
    }

}
